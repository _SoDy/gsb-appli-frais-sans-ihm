<?php
require_once '_head.traitement.inc.php';

if (isset($_REQUEST)) {
    $montant = $_REQUEST["montant"];
    $date = $_REQUEST["date"];
    $libelle = $_REQUEST["libelle"];
    $ficheFrais = obtenirUneFicheDeFraisDeUnVisiteurDeUnMoisDonne($_SESSION["user"]["idUser"], $moisAnnee = date('mY'));
    try {
        $dateTime = new DateTime($date);
    } catch (Exception $e) {
        $dateTime = new DateTime();
    }
    $ticks = $dateTime->getTimestamp();
    ajouterUneLigneDeFraisHorsForfaitDeUneFicheDeFrais($ficheFrais, $libelle, $ticks, $montant);

    header("Location:visiteur.saisieHorsForfait.php");
}




