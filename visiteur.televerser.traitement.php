<?php 

include_once '_head.traitement.inc.php'; 
include_once '_head.inc.php'; 

$repertoireDeApplication = getcwd();

$uploaddir = $repertoireDeApplication .'/justificatifs/' . $_POST["nomVisiteur"] . '/';
$uploadfile = $uploaddir . $_POST["idLigneFraisHorsForfait"];

if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
    if (!is_dir($uploaddir)) {
        mkdir($uploaddir,0777,true);
    }
    $reussi = move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile);
}

header("Location: visiteur.televerser.php");