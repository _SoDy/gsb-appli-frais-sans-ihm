<?php

define("DIR_APP", getcwd());

define("CONFIG", [
    "db" => [
        "db_engine"=>"mysql",
        "db_host"=>"localhost",
        "db_name" => "gsb-frais",
        "db_user" => "root",
        "db_password" => "Azerty1"
    ],
    "log" => [
        "db_file_log"=> DIR_APP.'/logs/db.log',
        "error_file.log" => DIR_APP.'logs/error.log'
    ]
]);

