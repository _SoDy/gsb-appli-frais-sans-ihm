<?php include_once '_head.traitement.inc.php'; ?>
<?php include_once '_head.inc.php'; ?>

<?php include_once '_comptable.menu.inc.php'; ?>

<?php
$idVisiteur = $_GET['idVisiteur'];
$visiteur = ObtenirVisiteur($idVisiteur);
$collectionFicheFrais = ObtenirLesFichesDeFraisEnCoursDeUnVisiteur($idVisiteur);
?>


<?php echo $visiteur["prenom"]; ?>
<?php echo $visiteur["nom"]; ?>

Fiche de frais à l'état en cours

<?php foreach ($collectionFicheFrais as $ficheFrais):
    ?>

    <?php
    $mois = substr($ficheFrais["moisAnnee"], 0, 2);
    $annee = substr($ficheFrais["moisAnnee"], 2, 4);
    echo $mois . " " . $annee;
    ?>

    <?php echo $ficheFrais["nbJustificatifs"]; ?>
    <?php echo $ficheFrais["montantValide"]; ?>
    <?php echo $ficheFrais["dateModif"]; ?>

    <?php
endforeach;
?>
