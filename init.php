<?php

include_once 'local/config.php';
$db_engine = CONFIG["db"]["db_engine"];
$db_host = CONFIG["db"]["db_host"];
$db_name = CONFIG["db"]["db_name"];
$db_user = CONFIG["db"]["db_user"];
$db_password = CONFIG["db"]["db_password"];

$pdo = new PDO("$db_engine:host=$db_host;dbname=$db_name", $db_user, $db_password,
        array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', 65536));

$req = "SELECT COUNT(*) as nbTables FROM information_schema.tables WHERE table_schema = '$db_name'";
$pdoStatement = $pdo->query($req);
$result = $pdoStatement->fetch();
if ($result["nbTables"] == 0) {
    $req = file_get_contents('script/gsb-frais.sql');
    $pdo->query($req);
}

header('Location: index.php');