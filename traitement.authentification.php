<?php

require_once '_head.traitement.inc.php';


$login = filter_input(INPUT_POST, "login",FILTER_SANITIZE_STRING);
$mdp = filter_input(INPUT_POST, "mdp",FILTER_SANITIZE_STRING);

if (authentification($login, $mdp)) {
    if ($_SESSION["user"]["profil"] == "Visiteur") {
        header("Location: visiteur.accueil.php");
    } else if ($_SESSION["user"]["profil"] == "Comptable") {
        header("Location: comptable.accueil.php");
    }
} else {
    header("Location: index.php");
}
