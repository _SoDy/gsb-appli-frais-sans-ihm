<?php include_once '_head.traitement.inc.php'; ?>
<?php
include_once '_head.inc.php';

$idVisiteur = $_SESSION["user"]["idUser"];
$moisAnnee = date('mY');
$ficheFrais = obtenirUneFicheDeFraisDeUnVisiteurDeUnMoisDonne($idVisiteur, $moisAnnee);
?>




<?php include_once '_visiteur.menu.inc.php'; ?>



Frais Hors Forfait



<form method="post" action="visiteur.traitement.saisieHorsForfait.php">

    <input type="hidden" name="idFicheFrais" id="idFicheFrais" value="<?php echo $ficheFrais["idFicheFrais"] ?>" />

    Libellé
    <input type="text" id="libelle" name="libelle"  placeholder="libellé" >


    Date
    <input id="date" name="date" pattern="(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)\d\d" type="text"  placeholder="jj-mm-aaaa" >
    

    Montant
    <input name="montant" id="montant" type="text" required pattern="[0-9]+(\.[0-9]+)?"  placeholder="€" >


    <button type="submit" >Valider</button>



</form>

<br><br>

Récapitulatif des frais hors forfait  
<br>
Libellé
Date
Montant

<br>

<?php
$collectionLigneFraisHorsForfait = obtenirCollectionDeLigneFraisHorsForfait($ficheFrais);
if ($collectionLigneFraisHorsForfait != null):

    foreach ($collectionLigneFraisHorsForfait as $ligneFraisHorsForfait) :
        ?>
        <?php echo date('d/m/Y', $ligneFraisHorsForfait["date"]) ?>
        <?php echo $ligneFraisHorsForfait["montant"] ?>
        <?php echo $ligneFraisHorsForfait["libelle"] ?>
        <a href="visiteur.traitement.suppressionLigneFraisHorsForfait.php?idFicheFrais=<?php echo $ligneFraisHorsForfait["idFicheFrais"] ?>&idLigneFraisHorsForfait=<?php echo $ligneFraisHorsForfait["idLigneFraisHorsForfait"] ?>"><span title="Supprimer" class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a></td>
        <br>
        <?php
    endforeach;
endif;
?>

<?php include_once '_footer.inc.php'; ?>
