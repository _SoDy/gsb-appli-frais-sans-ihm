PRESENTATION DE L'APPLICATION
=============================


>   Contexte : GSB   
>   Application Appli-Frais   

>   Module de gestion des notes de frais des visiteurs médicaux

>   Les interfaces graphiques ne sont pas implémentées 
 

INSTALLATION
============

***

* Depuis un terminal, exécutez les commandes ci-après: 

>
    cd /var/www/html
    git clone https://pvignard@bitbucket.org/_SoDy/gsb-appli-frais-sans-ihm.git gsb-frais
    cd gsb-frais
    rm -Rf .git/


* Renommez le fichier /var/www/html/gsb-frais/local/config.sample.php en /var/www/html/gsb-frais/local/config.php
   
* Vérifiez la section "db" (nom de la base de données, login et mot de passe pour l'accès aux bases de données) dans le fichier /var/www/html/gsb-frais/local/config.php

* Ouvrir le lien suivant dans votre navigateur : http://localhost/gsb-frais-td-bootstrap/init.php

* Depuis un terminal, exécutez les commandes ci-après:

>
    cd /var/www/html/gsb-frais
    mkdir justificatifs/ logs/ 
    sudo chown -R dev:www-data justificatifs/ logs/   #dev == votre nom d'utilisateur
      
 

INFORMATIONS
===========

***

>
    Comptes permettant une connexion à l'application avec l'un des deux profils de l'application  
    visiteur (login, mot de passe) :    
      dandre ; oppg5  
      pbentot ; doyw1
>
    comptable :     
     azed ; coppg5
>
    [Cf. table USER (autres comptes de connexion)]


