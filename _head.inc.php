<!DOCTYPE html>
<html lang="fr">
    <head>
        <script>
            function noBack() {
                window.history.forward()
            }
            noBack();
            window.onload = noBack;
            window.onpageshow = function (evt) {
                if (evt.persisted)
                    noBack()
            }
            window.onunload = function () {
                void(0)
            }
        </script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="">
        <meta name="author" content="pvig">
        <link rel="icon" href="../../favicon.ico">

        <?php if (preg_match('#comptable#', $_SERVER['PHP_SELF'])): ?>
            <link href="comptable.feuilleDeStyles.css" rel="stylesheet">
        <?php elseif (preg_match('#visiteur#', $_SERVER['PHP_SELF'])): ?>
            <link href="visiteur.feuilleDeStyles.css" rel="stylesheet">
            <?php
        endif;
        ?>

        <title>Module de gestion des notes de frais</title>

        <!-- Bootstrap core CSS -->
        <link href="./bootstrap334/css/bootstrap.min.css" rel="stylesheet">

    </head>
    <body>
