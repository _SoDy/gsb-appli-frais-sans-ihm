<?php
require_once '_head.traitement.inc.php';

if (isset($_REQUEST)) {
    $idFicheFrais = $_REQUEST["idFicheFrais"];
    foreach ($_REQUEST as $idFrais => $quantite) {
        if ($idFrais != "idFicheFrais") {
            modifierQuantiteDeUneLigneDeFicheFrais($idFrais, $idFicheFrais, $quantite);
        }
    }
}

header("Location: visiteur.saisieFicheFrais.php");

