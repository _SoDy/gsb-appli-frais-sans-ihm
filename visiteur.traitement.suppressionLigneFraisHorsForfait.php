<?php
require_once '_head.traitement.inc.php';

if (isset($_REQUEST["idLigneFraisHorsForfait"]) && isset($_REQUEST["idFicheFrais"])) {
    $idLigneFraisHorsForfait = $_REQUEST["idLigneFraisHorsForfait"];    
    supprimerUneLigneDeFraisHorsForfaitDeUneFicheDeFrais($idLigneFraisHorsForfait);   
}
header("location: visiteur.saisieHorsForfait.php");