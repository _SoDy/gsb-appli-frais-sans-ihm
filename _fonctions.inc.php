<?php

/**
 * Initialise une (variable de) connexion à la base de données
 * 
 * @return \PDO une instance d'objet PDO configuré
 */
function gestionnaireDeConnexion() {
    try {
        $pdo = new PDO(
                CONFIG["db"]["db_engine"] . ':host=' . CONFIG["db"]["db_host"] . ';dbname=' . CONFIG["db"]["db_name"],
                CONFIG["db"]["db_user"],
                CONFIG["db"]["db_password"],
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
        );
    } catch (PDOException $pdoException) {
        $error = __FUNCTION__ . "\n" .
                json_encode($pdoException->errorInfo) . "\n";
        error_log($error, 3, CONFIG["log"]["db_file_log"]);
        header("Location:erreur.php");
        exit();
    }
    return $pdo;
}

/**
 * Exécute une requête SQL de type SELECT via le gestionnaire de connexion
 * 
 * @param string $requete une requête SQL de sélection
 * @return array retourne un enregistrement
 */
function obtenirUnEnregistrement($requete): Array {
    $pdo = gestionnaireDeConnexion();
    try {
        $pdoStatement = $pdo->query($requete);
        $unEnregistrement = $pdoStatement->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $pdoException) {
        $error = __FUNCTION__ . "\n" .
                json_encode($pdoException->errorInfo) . "\n";
        error_log($error, 3, CONFIG["log"]["db_file_log"]);
        header("Location:erreur.php");
        exit();
    }
    return $unEnregistrement;
}

/**
 * Exécute une requête SQL de type SELECT via le gestionnaire de connexion
 * 
 * @param string $requete une requête SQL de sélection
 * @return array retourne un jeu d'enregistrements
 */
function obtenirTousLesEnregistrements($requete): Array {
    $pdo = gestionnaireDeConnexion();
    try {
        $pdoStatement = $pdo->query($requete);
        $lesEnregistrements = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $pdoException) {
        $error = __FUNCTION__ . "\n" .
                json_encode($pdoException->errorInfo) . "\n";
        error_log($error, 3, CONFIG["log"]["db_file_log"]);
        header("Location:erreur.php");
        exit();
    }
    return $lesEnregistrements;
}

/**
 * Exécute une requête SQL de type INSERT via le gestionnaire de connexion
 * 
 * @param string $requete une requête SQL d'insertion
 * @param bool $obtenirClePrimaire la fonction retourne la clé primaire de l'enregistrement créé 
 * @return int|bool le nombre d'enregistrements inséré ou la clé primaire du dernier enregistrement inséré si $obtenirClePrimaire est égal à true 
 */
function insererUnEnregistrement($requete, $obtenirClePrimaire = false) {
    $pdo = gestionnaireDeConnexion();
    try {
        $resultat = $pdo->exec($requete);
        if ($obtenirClePrimaire === true) {
            $resultat = $pdo->lastInsertId();
        }
    } catch (PDOException $pdoException) {
        $error = __FUNCTION__ . "\n" .
                json_encode($pdoException->errorInfo) . "\n";
        error_log($error, 3, CONFIG["log"]["db_file_log"]);
        header("Location:erreur.php");
        exit();
    }
    return $resultat;
}

/**
 * Exécute une requête SQL de type UPDATE via le gestionnaire de connexion
 * @param string $requete une requête SQL de mise à jour
 * @return int le nombre d'enregistrements mis à jour
 */
function mettreAJourUnEnregistrement($requete) {
    $pdo = gestionnaireDeConnexion();
    try {
        $resultat = $pdo->exec($requete);
    } catch (PDOException $pdoException) {
        $error = "FONCTION : ". __FUNCTION__ . "\n" .
                $pdoException->getTraceAsString() ."\n" .
                json_encode($pdoException->errorInfo) . "\n";
        error_log($error, 3, CONFIG["log"]["db_file_log"]);
        header("Location:erreur.php");
        exit();
    }
    return $resultat;
}

/**
 * Exécute une requête SQL de type DELETE via le gestionnaire de connexion
 * @param string $requete une requête SQL de suppression
 * @return int le nombre d'enregistrements supprimés
 */
function supprimerUnEnregistrement($requete) {
    $pdo = gestionnaireDeConnexion();
    try {
        $resultat = $pdo->exec($requete);
    } catch (PDOException $pdoException) {
        $error = __FUNCTION__ . "\n" .
                json_encode($pdoException->errorInfo) . "\n";
        error_log($error, 3, CONFIG["log"]["db_file_log"]);
        header("Location:erreur.php");
        exit();
    }
    return $resultat;
}

/**
 * Retourne l'ensemble des lignes d'enregistrements des frais forfaitaires d'une fiche de frais 
 * 
 * @remarks les informations sur les types de frais forfaitaires apparaissent pour chaque ligne de frais forfaitaire
 * @param array $ficheFrais Une fiche de frais sous la forme d'un enregistrement (un tuple issu de la base de données)
 * @return Array Un jeu d'enregistrement contenant l'ensemble des lignes de frais forfaitaires d'une fiche de frais
 */
function obtenirCollectionLigneFraisForfait(Array $ficheFrais) {

    $idFicheFrais = $ficheFrais["idFicheFrais"];

    $req = "select lignefraisforfait.* , fraisforfait.*
                from lignefraisforfait, fraisforfait
                where lignefraisforfait.idFraisForfait = fraisforfait.idFraisForfait
                and lignefraisforfait.idFicheFrais = '$idFicheFrais'";

    $lesLignesDeFraisForfait = obtenirTousLesEnregistrements($req);
    if (count($lesLignesDeFraisForfait) == 0) {
        initialiserLesLignesDeFraisDeUneFicheDeFrais($idFicheFrais);
        obtenirCollectionLigneFraisForfait($ficheFrais);
    }
    return $lesLignesDeFraisForfait;
}

/**
 * Vérifie l'existance d'un login et mot de passe saisis. Les données de l'utilisateur connecté sont sauvegardées dans une session php
 * 
 * @param type $login le login visiteur ou comptable
 * @param type $mdp le mot de passe visiteur ou comptable
 * @return boolean retourne vrai si le login et le mot de passe sont correctes, faux dans le cas contraire
 */
function authentification($login, $mdp) {
    $bool = false;
    $pdo = gestionnaireDeConnexion();
    $sql = "SELECT user.* FROM user WHERE login=:login AND mdp=:mdp";
    $prep = $pdo->prepare($sql);
    $prep->bindParam(':login', $login, PDO::PARAM_STR);
    $prep->bindParam(':mdp', $mdp, PDO::PARAM_STR);
    $resultat = $prep->execute();
    $resultat = $prep->fetch(PDO::FETCH_ASSOC);
    if (count($resultat) > 0) {
        $bool = true;
        $_SESSION["user"] = $resultat;
    }
    $prep->closeCursor();

    return $bool;
}

/**
 * Modifie la quantité associée à une ligne de frais forfaitaire
 * 
 * @param type $idFraisForfait l'idendifiant de la ligne de frais forfaitaire
 * @param type $idFicheFrais l'identifiant de la fiche de frais
 * @param type $quantite la quantité à modifiée
 */
function modifierQuantiteDeUneLigneDeFicheFrais($idFraisForfait, $idFicheFrais, $quantite) {
    $req = "update lignefraisforfait set quantite='$quantite'"
            . " where idFraisForfait='$idFraisForfait' and idFicheFrais='$idFicheFrais'";
    mettreAJourUnEnregistrement($req);
}

/**
 * Retourne l'ensemble des lignes d'enregistrements des frais hors forfait d'une fiche de frais
 * 
 * @param array $ficheFrais Une fiche de frais sous la forme d'un enregistrement (un tuple issu de la base de données)
 * @return Array Un jeu d'enregistrement contenant l'ensemble des lignes de frais hors forfait d'une fiche de frais
 */
function obtenirCollectionDeLigneFraisHorsForfait(Array $ficheFrais) {

    $idFicheFrais = $ficheFrais["idFicheFrais"];

    $req = "select lignefraishorsforfait.* 
                from lignefraishorsforfait, fichefrais
                where fichefrais.idFicheFrais = lignefraishorsforfait.idFicheFrais
                and fichefrais.idFicheFrais = '$idFicheFrais' ";

    $lesLignesDeFraisHorsForfait = obtenirTousLesEnregistrements($req);
    return $lesLignesDeFraisHorsForfait;
}

/**
 * Retourne une fiche de frais
 * 
 * @param type $idVisiteur L'identifiant d'un visiteur
 * @param type $moisAnnee Le mois et l'année d'une fiche de frais
 * @return Array Une fiche de frais sous la forme d'un enregistrement
 */
function obtenirUneFicheDeFraisDeUnVisiteurDeUnMoisDonne($idVisiteur, $moisAnnee) {

    $req = "  select fichefrais.* "
            . " from fichefrais "
            . " where fichefrais.moisAnnee = '" . $moisAnnee . "' "
            . " and fichefrais.idVisiteur = '" . $idVisiteur . "'";
    $ficheFrais = obtenirUnEnregistrement($req);

    if ($ficheFrais == false) {
        $req = " insert into fichefrais (moisAnnee,idVisiteur,idEtat) "
                . " values ('$moisAnnee','$idVisiteur','EC') ";
        $idFicheFrais = insererUnEnregistrement($req, true);
        initialiserLesLignesDeFraisDeUneFicheDeFrais($idFicheFrais);
        return obtenirUneFicheDeFraisDeUnVisiteurDeUnMoisDonne($idVisiteur, $moisAnnee);
    }

    return $ficheFrais;
}

/**
 * Initialise les lignes de frais forfaits d'une fiche de frais
 * 
 * @param string|int $idFicheFrais Un identifiant de fiche de frais
 */
function initialiserLesLignesDeFraisDeUneFicheDeFrais($idFicheFrais) {
    $typeFraisForfait = null;

    $req = "  select fraisforfait.* from fraisforfait ";
    $typeFraisForfait = obtenirTousLesEnregistrements($req);

    foreach ($typeFraisForfait as $unType) {
        $idFraisForfait = $unType["idFraisForfait"];
        $req = " insert into lignefraisforfait (idFraisForfait,idFicheFrais,quantite) "
                . " values ('$idFraisForfait','$idFicheFrais','0') ";
        insererUnEnregistrement($req);
    }
}

/**
 * Ajoute une ligne de frais hors forfait à une fiche de frais
 * @param Array $ficheFrais Une fiche de frais sous la forme d'un enregistrement
 * @param type $libelle Le libellé d'un frais hors forfait
 * @param type $date La date du frais hors forfait
 * @param type $montant Le montant du frais hors forfait
 */
function ajouterUneLigneDeFraisHorsForfaitDeUneFicheDeFrais(Array $ficheFrais, $libelle, $date, $montant) {
    $idFicheFrais = $ficheFrais["idFicheFrais"];
    $req = " insert into lignefraishorsforfait (idFicheFrais,date,montant,libelle) "
            . " values ('$idFicheFrais','$date','$montant','$libelle') ";
    insererUnEnregistrement($req);
}

/**
 * Supprime une ligne de frais hors forfait
 * 
 * @param type $idLigneFraisHorsForfait L'identifiant d'une ligne de frais hors forfait
 */
function supprimerUneLigneDeFraisHorsForfaitDeUneFicheDeFrais($idLigneFraisHorsForfait) {
    $req = " delete from lignefraishorsforfait "
            . "where idLigneFraisHorsForfait = '$idLigneFraisHorsForfait' ";
    supprimerUnEnregistrement($req);
}

/**
 * Retourne l'ensemble des fiches de frais d'un visiteur
 * @param type $idVisiteur L'identifiant d'un visiteur
 * @return Array L'ensemble des fiches de frais sous la forme d'enregistrement
 */
function obtenirCollectionFicheDeFraisDeUnVisiteur($idVisiteur) {
    $req = "  select fichefrais.* , etat.* "
            . " from fichefrais, etat "
            . " where fichefrais.idEtat = etat.idEtat "
            . " and fichefrais.idVisiteur = '" . $idVisiteur . "'";
    $collectionFicheFrais = obtenirTousLesEnregistrements($req);
    return $collectionFicheFrais;
}

/**
 * Retourne une fiche de frais 
 * @param type $idFicheFrais L'identifiant d'une fiche de frais
 * @return Array|NULL Une fiche de frais sous la forme d'un enregistrements. Null si l'identifiant n'est pas lié à une fiche de frais
 */
function obtenirUneFicheDeFraisAvecIdentifiant($idFicheFrais) {

    $req = "  select fichefrais.* "
            . " from fichefrais "
            . " where fichefrais.idFicheFrais = '" . $idFicheFrais . "'";
    $ficheFrais = obtenirUnEnregistrement($req);
    return $ficheFrais;
}

/**
 * Retourne l'ensemble des fiches de frais comprenant des frais hors forfait d'un utilisateur donnée 
 * @param string $idVisiteur l'identifiation d'un utilisateur
 * @return Array une collection de fiche de frais
 */
function obtenirCollectionFicheFraisAvecFraisForfaitDeUnUtilisateur($idVisiteur) {
    $req = "  select fichefrais.* "
            . " from fichefrais"
            . " where fichefrais.idFicheFrais in (select fichefrais.idFicheFrais "
            . " from fichefrais,lignefraishorsforfait"
            . " where lignefraishorsforfait.idFicheFrais = fichefrais.idFicheFrais"
            . " and fichefrais.idVisiteur = '" . $idVisiteur . "')";
    $collectionFicheFrais = obtenirTousLesEnregistrements($req);
    return $collectionFicheFrais;
}

/**
 * Retourne une collection de ligne de frais hors forfait d'une fiche de frais
 * @param string $idFicheFrais l'identifiant d'une fiche de frais
 * @return Array une collection de ligne de frais hors forfait
 */
function obtenirCollectionLigneFraisHorsForfaitDeUneFicheFrais($idFicheFrais) {
    $req = "select lignefraishorsforfait.* , fichefrais.*
                from lignefraishorsforfait, fichefrais
                where fichefrais.idFicheFrais = lignefraishorsforfait.idFicheFrais
                and fichefrais.idFicheFrais = '$idFicheFrais'";
    $collectionLigneFraisHorsForfait = obtenirTousLesEnregistrements($req);
    return $collectionLigneFraisHorsForfait;
}

/**
 * Retourne une date au format JJ/MM/AAAA à partir d'un timestamp
 * @param string $timeStamp une date au format timestamp
 * @return string date
 */
function timestampToDate($timeStamp) {
    return date('d/m/Y', $timeStamp);
}

/**
 * Retourne les visiteurs qui ont des fiches de frais à l'état en cours
 * @return Array une collection de visiteurs ayant au moins une fiche de frais à l'état en cours
 */
function ObtenirCollectionDeVisiteursAvecFicheFraisEnCours() {

    $req = " select distinct A.* "
            . " from user A "
            . " inner join fichefrais B "
            . " on B.idVisiteur = A.idUser "
            . " and B.idEtat = 'EC'"
            . " and B.idFicheFrais IS NOT NULL";

    $collectionVisiteurs = obtenirTousLesEnregistrements($req);
    return $collectionVisiteurs;
}

/**
 * Retourne les fiches de frais à l'état en cours d'un visiteur
 * 
 * @param type $idVisiteur L'identifiant d'un visiteur
 * @return Array Collection des fiches de frais à l'état en cours d'un visiteur
 */
function ObtenirLesFichesDeFraisEnCoursDeUnVisiteur($idVisiteur) {

    $req = "  select fichefrais.* "
            . " from fichefrais "
            . " where fichefrais.idVisiteur = '" . $idVisiteur . "'"
            . " and fichefrais.idEtat = 'EC' ";
    $collectionFicheFrais = obtenirTousLesEnregistrements($req);
    return $collectionFicheFrais;
}

/**
 * Retourne un visiteur
 * @param type $idVisiteur L'identifiant d'un visiteur
 * @return type Array Un visiteur
 */
function ObtenirVisiteur($idVisiteur) {

    $req = "  select user.* "
            . " from user "
            . " where user.idUser = '" . $idVisiteur . "'";
    $visiteur = obtenirUnEnregistrement($req);
    return $visiteur;
}
